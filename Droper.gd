extends Sprite2D

var fruta = preload("res://fruta.tscn")
var rng

var drop_pos = Vector2(0, 20)
var can_drop = true
var touching = false
var next_to_drop = null
var queue = [1,2] # range(1,12) #

signal next_drop

# Called when the node enters the scene tree for the first time.
func _ready():
	rng = RandomNumberGenerator.new()
	drop_ready()
	
func _input(event):
	if event is InputEventMouseMotion:
		# While dragging, move the sprite with the mouse.
		position.x = event.position.x
	if event is InputEventMouseButton:
		if event.button_index == MOUSE_BUTTON_LEFT and event.pressed:
			drop()

	if event is InputEventScreenDrag:
		position.x = event.position.x
	if event is InputEventScreenTouch:
		position.x = event.position.x
		if not event.pressed and touching:
			drop()
			touching = false
		touching = true

func drop_ready():
	next_to_drop = fruta.instantiate()
	next_to_drop.start(queue.pop_front())
	next_to_drop.position = drop_pos
	next_to_drop.freeze = true
	add_child(next_to_drop)
	queue.push_back(rng.randi_range(1, 5))
	emit_signal("next_drop", queue[0])

func drop():
	if not can_drop:
		return
	can_drop = false
	$RefreshDrop.start()
	
	next_to_drop.freeze = false
	remove_child(next_to_drop)
	get_parent().add_child(next_to_drop)
	next_to_drop.global_position = global_position + drop_pos

func _on_refresh_drop_timeout():
	can_drop = true
	drop_ready()
	$RefreshDrop.stop()
